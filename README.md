

# **Verywear API Send SMS**

##  Manage informations about API send SMS

### URL

	http://localhost:8080/v1/sms

### Method:
  
  `GET`,`POST` 
  
### Documentation:
 Documentation portail is available on : http://localhost:8080/swagger-ui.html

### Docker Build on your machine

	onto your directory projet :
	docker build --rm=true --no-cache --label  --build-arg JAR_FILE=target/{projectName}-{version}.jar -t {projectName} .

## Use it

### [POST] post a SMS : 
url : http://localhost:8080/v1/sms

template content of body
 ```json
{ "from": "sender mail into this list :  DEVIANNE, DMV, VERYWEAR, JULIE&CO, STANFORD", 
  "to": "recipient with the dialing code",
  "message": "Content Message with length < 160 characters"
}
```

Example :
 ```json
{ "from": "DEVIANNE",
  "to": "+33611223344",
  "message": "Ceci est un test"}
 ```
 
 
### [GET] Number of SMS available: 
url : http://localhost:8080/v1/sms
 ```json
{  
  "creditSms": 2142
}
```


 