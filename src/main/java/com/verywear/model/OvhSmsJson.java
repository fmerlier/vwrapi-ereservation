package com.verywear.model;

import java.util.List;

public class OvhSmsJson {

	private String charset = "UTF-8";
	private String coding = "7bit";
	private String message;
	private Boolean noStopClause = true;
	private String priority = "high";
	private List<String> receivers;
	private String sender = "DMV";
	private Boolean senderForResponse = false;
	private Integer validityPeriod = 2880;

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getCoding() {
		return coding;
	}

	public void setCoding(String coding) {
		this.coding = coding;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getNoStopClause() {
		return noStopClause;
	}

	public void setNoStopClause(Boolean noStopClause) {
		this.noStopClause = noStopClause;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public List<String> getReceivers() {
		return receivers;
	}

	public void setReceivers(List<String> receivers) {
		this.receivers = receivers;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public Boolean getSenderForResponse() {
		return senderForResponse;
	}

	public void setSenderForResponse(Boolean senderForResponse) {
		this.senderForResponse = senderForResponse;
	}

	public Integer getValidityPeriod() {
		return validityPeriod;
	}

	public void setValidityPeriod(Integer validityPeriod) {
		this.validityPeriod = validityPeriod;
	}

}
