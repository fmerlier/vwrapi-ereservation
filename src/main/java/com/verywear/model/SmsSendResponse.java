/**
 *
 */
package com.verywear.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Data;

@Data
public class SmsSendResponse {

	@JacksonXmlProperty( localName = "sent", isAttribute = false)
	@JsonProperty("sent")
	private boolean envoye;

}
