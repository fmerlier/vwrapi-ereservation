/**
 *
 */
package com.verywear.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Data;

@Data
public class SmsCreditResponse {

	@JacksonXmlProperty( localName = "creditSms", isAttribute = false)
	@JsonProperty("creditSms")
	private int nombreSMSRestant;

}
