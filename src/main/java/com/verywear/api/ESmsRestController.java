package com.verywear.api;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.verywear.model.SmsRequest;
import com.verywear.model.SmsSendResponse;
import com.verywear.model.SmsCreditResponse;
import com.verywear.service.SmsService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/v1/sms", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = { MediaType.ALL_VALUE, MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
public class ESmsRestController {

	private static Log log = LogFactory.getLog(ESmsRestController.class);
	
	@Autowired
	SmsService smsService;

	@PostMapping("")
	@ApiOperation(value = "send SMS API", notes = "Send a sms")
	@ApiParam
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully send sms"), @ApiResponse(code = 400, message = "Parameters issue occurred"), @ApiResponse(code = 500, message = "An internal error has occurred") })
	public ResponseEntity<Object> sendSms(@RequestBody final SmsRequest paramSms) {

		if (paramSms.getFrom() == null || ("").equals(paramSms.getFrom())) {
			return new ResponseEntity<>("The parameter 'from' is empty", HttpStatus.BAD_REQUEST);
		}
		if ((paramSms.getTo()==null) || (paramSms.getTo().isEmpty())) {
			return new ResponseEntity<>("The parameter 'to' is empty", HttpStatus.BAD_REQUEST);
		}
		//Check le format des destinataires +33699073545
		if (!smsService.checkPhoneNumber(paramSms.getTo())) {
			return new ResponseEntity<>("The parameter 'to' is not format correctly", HttpStatus.BAD_REQUEST);
		}		
		if (paramSms.getMessage()==null || ("").equals(paramSms.getMessage().trim())) {
			return new ResponseEntity<>("The parameter 'message' is empty", HttpStatus.BAD_REQUEST);
		}
		if (paramSms.getMessage().trim().length() > 160) {
			return new ResponseEntity<>("The parameter 'message' is too long", HttpStatus.BAD_REQUEST);
		}
		
		log.debug("SMS envoyé : De="+paramSms.getFrom()+" A="+paramSms.getTo()+" Message="+paramSms.getMessage());
		final Boolean isSent = smsService.send(paramSms);
		SmsSendResponse smsSendResponse = new SmsSendResponse();
		smsSendResponse.setEnvoye(isSent);
		
		if (Boolean.TRUE.equals(isSent)) {
			return new ResponseEntity<>(smsSendResponse, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(smsSendResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@GetMapping("")
	@ApiOperation(value = "credit information SMS API", notes = "Get information about sms service")
	@ApiParam
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully get credit information sms"), @ApiResponse(code = 400, message = "An error has occurred"), @ApiResponse(code = 500, message = "An internal error has occurred") })
	public ResponseEntity<SmsCreditResponse> informationSms() {

		int nombreSmsRestants = smsService.controleNombreSMSRestantSurLeCompte();
		
		log.debug("Nombre de SMS restant: "+nombreSmsRestants);
		
		if (nombreSmsRestants != -1) {
			SmsCreditResponse smsResponse= new SmsCreditResponse();
			smsResponse.setNombreSMSRestant(nombreSmsRestants);
			return new ResponseEntity<>(smsResponse, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
//	();

}
