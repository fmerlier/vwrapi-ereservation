package com.verywear.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.verywear.model.OvhSmsJson;
import com.verywear.model.SmsRequest;

@Service
public class SmsService {

	private static Log log = LogFactory.getLog(SmsService.class);

	@Value("${sms.sender.service.name:sms-pj41281-1}")
	private String ServiceName;
	@Value("${sms.sender.app.url:https://eu.api.ovh.com/1.0}")
	private String querySmsUrl;
	@Value("${sms.sender.app.key:GqkOkVRsnpvb6Kva}")
	private String ApplicationKey;
	@Value("${sms.sender.app.secret:XVEOWjpnFLiseNeWgIyUMTK5t04OA8h9}")
	private String ApplicationSecret;
	@Value("${sms.sender.consumer.key:uqeScbzJ1TJNfwQKN8rIvMa1iQgCzHfr}")
	private String ConsumerKey;

	/* Pour changer les droits OVH sur un token: */
	// On peut tout retrouver sur l'api /me/agreements/...
	// appel:
	// curl -XPOST -H"X-Ovh-Application: GqkOkVRsnpvb6Kva" -H "Content-type: application/json" https://eu.api.ovh.com/1.0/auth/credential -d '{"accessRules": [{"method":
	// "GET","path": "/sms/*"},{"method": "POST","path": "/sms/*"},{"method": "PUT","path": "/sms/*"},{"method": "DELETE","path": "/sms/*"}],"redirection":""}'
	// réponse:
	// {"validationUrl":"https://eu.api.ovh.com/auth/?credentialToken=DOl3qaNbZcFsWTTVsWxh4i9mPdkwI4e4AttKdjKzP95Q7K3SgGFLTJLgXwtya4PO","consumerKey":"uqeScbzJ1TJNfwQKN8rIvMa1iQgCzHfr","state":"pendingValidation"}

	private Long deltaTimeStampWithOVH = null;
	private Date dateDerniereSynchronisationTimeStamp = null;

	public boolean send(SmsRequest paramSms) {
		boolean result = true;
		try {
			final String querySMS = querySmsUrl + "/sms/" + ServiceName + "/jobs/";
			final String METHOD = "POST";

			final String message = paramSms.getMessage().trim().replaceAll("\n|\r", "");
			final String receiver = paramSms.getTo();
			final Gson gson = new Gson();
			final OvhSmsJson ovhSmsJson = new OvhSmsJson();
			//On parametre l'emetteur
			ovhSmsJson.setSender(paramSms.getFrom());
			//On parametre les destinataires
			final List<String> jreceiver = new ArrayList<String>();
			jreceiver.add(receiver);
			ovhSmsJson.setReceivers(jreceiver);
			//On parametre le message
			ovhSmsJson.setMessage(new String(message.substring(0, Math.min(160, message.length())).getBytes("UTF-8"), "ISO-8859-1"));
			final String BODY = gson.toJson(ovhSmsJson);

			// On analyse la reposne
			final String reponse = callAPIOvh(querySMS, METHOD, BODY);
			final JsonElement je = new JsonParser().parse(reponse);
			final JsonObject jo = je.getAsJsonObject();
			final float nombreSmsEnvoyes = jo.getAsJsonPrimitive("totalCreditsRemoved").getAsFloat();

			if (nombreSmsEnvoyes <= 0) {
				throw new Exception("erreur lors de l'envoi du sms " + paramSms + " : " + reponse);
			}

		} catch (final Exception e) {
			log.error(e.getMessage() + "erreur lors de l'envoi du sms " + paramSms, e);
			result = false;
		}
		return result;
	}

	public int controleNombreSMSRestantSurLeCompte() {
		int nombreSmsRestants = 0;
		try {
			final String querySMS = querySmsUrl + "/sms/" + ServiceName;
			final String METHOD = "GET";

			// On analyse la reposne
			final String reponse = callAPIOvh(querySMS, METHOD, null);
			final JsonElement je = new JsonParser().parse(reponse);
			final JsonObject jo = je.getAsJsonObject();
			nombreSmsRestants =  jo.getAsJsonPrimitive("creditsLeft").getAsBigDecimal().intValue();
//			log.info("Reste " + nombreSmsRestants + " sms à envoyer");
//
//			if (nombreSmsRestants < 500) {
//				log.fatal("RECHARGER les crédits SMS OVH, reste " + nombreSmsRestants + " sms à envoyer");
//			}
//			if (nombreSmsRestants < 1) {
//				throw new Exception("RECHARGER les crédits SMS OVH, reste " + nombreSmsRestants + " sms à envoyer");
//			}			
		} catch (final Exception e) {
			log.error("Problème d'appel à l'API pour connaitre le crédit restant");
			nombreSmsRestants = -1;
		}
		return nombreSmsRestants;
	}

	private String callAPIOvh(final String url, final String method, final String body) throws Exception {
		try {
			final URL querySMS = new URL(url);
			// J'ajoute 5 minute de decalage systeme, genant pour l'encryptage
			// Recuperer le timestamp chez OVH
			// https://eu.api.ovh.com/1.0/auth/time puis calculer l'ecart
			final long TSTAMP = new Date().getTime() / 1000 + callTimeSynchronisation();

			// Creation de la signature
			final String toSign = ApplicationSecret + "+" + ConsumerKey + "+" + method + "+" + url + "+" + (body == null ? "" : body) + "+" + TSTAMP;
			final String signature = "$1$" + HashSHA1(toSign);

			final HttpURLConnection smsSvc = (HttpURLConnection) querySMS.openConnection();
			smsSvc.setRequestMethod(method);
			smsSvc.setRequestProperty("Accept-Charset", "UTF-8");
			smsSvc.setRequestProperty("Content-Type", "application/json");
			smsSvc.setRequestProperty("X-Ovh-Application", ApplicationKey);
			smsSvc.setRequestProperty("X-Ovh-Consumer", ConsumerKey);
			smsSvc.setRequestProperty("X-Ovh-Signature", signature);
			smsSvc.setRequestProperty("X-Ovh-Timestamp", "" + TSTAMP);
			smsSvc.setRequestProperty("Connection", "close");
			if (!(body == null || "".equals(body))) {
				smsSvc.setDoOutput(true);
				final DataOutputStream wr = new DataOutputStream(smsSvc.getOutputStream());
				final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr, "ISO-8859-1"));
				writer.write(body);
				writer.flush();
				wr.close();
			}

			String inputLineSms;
			BufferedReader inSms;
			final int httpResponseCode = smsSvc.getResponseCode();
			if (httpResponseCode == HttpURLConnection.HTTP_OK) {
				// Récupération du résultat de l'appel
				inSms = new BufferedReader(new InputStreamReader(smsSvc.getInputStream(), "UTF-8"));
				final StringBuffer response = new StringBuffer();
				while ((inputLineSms = inSms.readLine()) != null) {
					response.append(inputLineSms);
				}
				inSms.close();
				smsSvc.disconnect();

				return response.toString();

			} else {
				inSms = new BufferedReader(new InputStreamReader(smsSvc.getErrorStream(), "UTF-8"));
				final StringBuffer response = new StringBuffer();
				while ((inputLineSms = inSms.readLine()) != null) {
					response.append(inputLineSms);
				}
				log.fatal("Erreur lors de l'envoi du SMS, http_status_code= " + httpResponseCode + "reponse=" + response.toString());
				throw new Exception("Erreur lors de l'envoi du SMS HTTP_status_code=" + httpResponseCode);
			}
		} catch (final Exception e) {
			log.error(e.getMessage() + "erreur lors de l'interrogation du credits de sms ", e);
			throw e;
		}
	}

	private long callTimeSynchronisation() {
		final GregorianCalendar hier = new GregorianCalendar();
		hier.add(GregorianCalendar.HOUR, -24);
		if (deltaTimeStampWithOVH != null) {
			if (dateDerniereSynchronisationTimeStamp.after(hier.getTime())) {
				return deltaTimeStampWithOVH;
			}
		}
		try {
			// On appel le service pour connaitre le decalage
			final URL queryTime = new URL(querySmsUrl + "/auth/time");
			URLConnection smsService;
			String retour = "";
			long OVHTimestamp = 0;
			long DMVTimestamp = 0;
			smsService = queryTime.openConnection();
			final BufferedReader in = new BufferedReader(new InputStreamReader(smsService.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				retour += inputLine;
			}
			in.close();
			OVHTimestamp = Long.parseLong(retour);
			DMVTimestamp = new Date().getTime() / 1000;
			// On calcul le delta timestamp pour l'ajouter ensuite
			deltaTimeStampWithOVH = OVHTimestamp - DMVTimestamp;
			dateDerniereSynchronisationTimeStamp = new Date();
		} catch (final Exception e) {
			log.fatal("Impossible de synchroniser les horloges", e);
			return 0;
		}
		return deltaTimeStampWithOVH;
	}

	public String HashSHA1(final String text) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-1");
			byte[] sha1hash = new byte[40];
			md.update(text.getBytes("iso-8859-1"), 0, text.length());
			sha1hash = md.digest();
			return convertToHex(sha1hash);
		} catch (final NoSuchAlgorithmException e) {
			final String errmsg = "NoSuchAlgorithmException: " + text + " " + e;
			return errmsg;
		} catch (final UnsupportedEncodingException e) {
			final String errmsg = "UnsupportedEncodingException: " + text + " " + e;
			return errmsg;
		}
	}

	private String convertToHex(final byte[] data) {
		final StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = data[i] >>> 4 & 0x0F;
			int two_halfs = 0;
			do {
				if (0 <= halfbyte && halfbyte <= 9) {
					buf.append((char) ('0' + halfbyte));
				} else {
					buf.append((char) ('a' + (halfbyte - 10)));
				}
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	public boolean checkPhoneNumber(String phoneNumber) {
		//TODO FME
		return true;
	}

}
