package com.verywear.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableAutoConfiguration
public class SwaggerConfig {

	@Value("${spring.application.name}")
	private String applicationName;

	@Bean
	public Docket api() {

		Docket docket = new Docket(DocumentationType.SWAGGER_2);
		docket = docket.select().apis(RequestHandlerSelectors.basePackage("com.verywear")).paths(PathSelectors.any()).build();
		docket.groupName(applicationName);
		docket.apiInfo(metaData());
		return docket;

	}

	@Bean
	public Docket springBootActuator() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2);
		docket = docket.select().apis(RequestHandlerSelectors.basePackage("org.springframework.boot.actuate")).paths(PathSelectors.any()).build();
		docket.groupName("spring-actuator");
		return docket;

	}

	private ApiInfo metaData() {
		return new ApiInfoBuilder().title("Spring Boot REST API")
				.description("\"REST API for SMS\"")
				.version("1.0.0")
				.license("Verywear")
				.licenseUrl(null)
				.contact(new Contact("DSIO Verywear", null, "etudes@verywear"))
				.build();
	}

}
